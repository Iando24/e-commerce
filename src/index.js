import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Footer from './Includes/Footer/Footer';
import NavBar from './Includes/NavBar/NavBar';
import * as serviceWorker from './serviceWorker';
import Rechecher from './Includes/Recherche/Rechercher';
import Triage from './Includes/Triage/Triage';
import MenuGauche from './Includes/MenuGauche/MenuGauche';

ReactDOM.render(<NavBar />, document.getElementById('navbar'));
ReactDOM.render(<Rechecher />, document.getElementById('rechercher'))
ReactDOM.render(<Triage />, document.getElementById('triage'))
ReactDOM.render(<MenuGauche />, document.getElementById('menuGauche'))
ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(<Footer />, document.getElementById('footer'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

