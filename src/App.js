import React, { Component } from 'react';
import bg1 from './Images/sliders/slider1.png';
import './App.css';

class App extends Component {
  render(){
    return  <div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder">
              <div id="hero" class="homepage-slider3">
                <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm owl-theme">
                  <div class="owl-wrapper-outer">
                    <div class="owl-wrapper">
                      <div class="owl-item active">
                        <div class="full-width-slider">
                          <div class="item" style={{ backgroundImage: `url(${bg1})` }} ></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
  }
}

export default App;
