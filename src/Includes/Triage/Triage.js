import React, { Component } from 'react';

import './Triage.css';

class Triage extends Component{
    render(){
        return <div class="header-nav animate-dropdown">
                    <div class="container">
                        <div class="yamm navbar navbar-default" role="navigation">
                            <div class="navbar-header">
                                <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="nav-bg-class">
                                <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
                                    <div class="nav-outer">
                                        <ul class="nav navbar-nav">
                                            <li class="active dropdown yamm-fw">
                                                <a href="index-2.html" data-hover="dropdown" class="dropdown-toggle">Accueil</a>
                                            </li>
                                                                        <li class="dropdown yamm">
                                                <a href="#"> Hi-Tech</a>
                
                                            </li>
                                                                        <li class="dropdown yamm">
                                                <a href="#"> Scolaire &amp; bureau</a>
                
                                            </li>
                                                                        <li class="dropdown yamm">
                                                <a href="#"> Nourriture</a>
                
                                            </li>
                                                                        <li class="dropdown yamm">
                                                <a href="#"> Art Malagasy</a>
                
                                            </li>
                                                                        <li class="dropdown yamm">
                                                <a href="#"> Habillages &amp; Accessoires</a>
                
                                            </li>
                                                                        <li class="dropdown yamm">
                                                <a href="#"> Instruments de musique</a>
                
                                            </li>
                                            
                                        </ul>
                                        <div class="clearfix"></div>				
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    }
}

export default Triage;