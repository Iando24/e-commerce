import React, { Component } from 'react';
import './MenuGauche.css';

class MenuGauche extends Component{
    render(){
        return  <div class="col-xs-12 col-sm-12 col-md-3 sidebar">
                    <div class="side-menu animate-dropdown outer-bottom-xs">
                        <div class="head"><i class="icon fa fa-align-justify fa-fw"></i> Categories</div>        
                        <nav class="yamm megamenu-horizontal" role="navigation">

                            <ul class="nav">
                                <li class="dropdown menu-item">
                                    <a href="#" class="dropdown-toggle"><i class="fa fa-caret-square-o-right" aria-hidden="true"></i>Hi-Tech</a>
                                    <a href="#" class="dropdown-toggle"><i class="fa fa-caret-square-o-right" aria-hidden="true"></i>Scolaire &amp; bureau</a>
                                    <a href="#" class="dropdown-toggle"><i class="fa fa-caret-square-o-right" aria-hidden="true"></i>Nourriture</a>
                                    <a href="#" class="dropdown-toggle"><i class="fa fa-caret-square-o-right" aria-hidden="true"></i>Art Malagasy</a>
                                    <a href="#" class="dropdown-toggle"><i class="fa fa-caret-square-o-right" aria-hidden="true"></i>Habillages &amp; Accessoires</a>
                                    <a href="#" class="dropdown-toggle"><i class="fa fa-caret-square-o-right" aria-hidden="true"></i>Instruments de musique</a>
                                                
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
    }
}

export default MenuGauche;