import React, { Component } from 'react';

import './Rechercher.css';

class Rechercher extends Component{
    render(){
        return  <div class="main-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-3 logo-holder">
                                <div class="logo">
                                    <a href="#"> 
                                        <img src="./img/logo.png" width="20%" alt="ASA Shop" />
                                    </a>
                                </div>		
                            </div>
							<div class="col-xs-12 col-sm-12 col-md-6 top-search-holder">
								<div class="search-area">
									<form name="search" method="post" action="http://asashopmada.com/search-result.php">
										<div class="control-group">
											<input class="search-field" placeholder="Taper votre texte ici..." name="product" required="required" />
											<button class="search-button" type="submit" name="search"></button>  
										</div>
									</form>
								</div>			
							</div>
							<div class="col-xs-12 col-sm-12 col-md-3 animate-dropdown top-cart-row">
								<div class="dropdown dropdown-cart">
									<a href="#" class="dropdown-toggle lnk-cart" data-toggle="dropdown">
										<div class="items-cart-inner">
											<div class="total-price-basket">
												<span class="lbl">Panier -</span>
												<span class="total-price">
													<span class="sign">Ar.</span>
													<span class="value">00.00</span>
												</span>
											</div>
											<div class="basket">
												<i class="glyphicon glyphicon-shopping-cart"></i>
											</div>
											<div class="basket-item-count"><span class="count">0</span></div>
										</div>
									</a>
									<ul class="dropdown-menu">
										<li>
											<div class="cart-item product-summary">
												<div class="row">
													<div class="col-xs-12">
														Votre panier est vide.
													</div>
												</div>
											</div>
											<hr/>
											<div class="clearfix cart-total">
												
												<div class="clearfix"></div>
													
												<a href="index-2.html" class="btn btn-upper btn-primary btn-block m-t-20">Continuer vos achats</a>	
											</div>
										</li>
									</ul>
								</div>
							</div>
							
                        </div>
                    </div>
                </div>
    }
}

export default Rechercher;