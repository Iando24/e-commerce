import React, { Component } from 'react';
import './NavBar.css';

class NavBar extends Component {
  render(){
    return  <div class="top-bar animate-dropdown">
              <div class="container">
                <div class="header-top-inner">
                  <div class="cnt-account">
                    <ul class="list-unstyled">
                      <li>
                        <a href="#"><i class="icon fa fa-user"></i>Bienvenue test </a>
                      </li>
                      <li>
                        <a href="#"><i class="icon fa fa-user"></i>Mon compte</a>
                      </li>
                      <li>
                        <a href="#"><i class="icon fa fa-heart"></i>Favoris</a>
                      </li>
                      <li>
                        <a href="#"><i class="icon fa fa-shopping-cart"></i>Mon panier</a>
                      </li>
                      <li>
                        <a href="#"><i class="icon fa fa-key"></i>Check-out</a>
                      </li>
                      <li>
                        <a href="#"><i class="icon fa fa-sign-in"></i>S'identifier</a>
                      </li>
                      {/* <li>
                        <a href="#"><i class="icon fa fa-sign-out"></i>D&eacute;connecter</a>
                      </li>
                      <li>
                        <a href="#" class="dropdown-toggle" >
                          <span class="key">
                            <i class="icon fa fa-angle-right"></i>Suivi de commande</span>
                        </a>
                      </li> */}
                    </ul>
                  </div>
                  <div class="cnt-block">
                    <ul class="list-unstyled list-inline">
                      <li class="dropdown dropdown-small">
                        <a class="dropdown-toggle" ><span class="key">T&eacute;l&eacute;phone: +261 034 81 84 201</span> &nbsp;&nbsp;<span class="key">, E-mail: commercial@asashop.com</span></a>
                      </li> 
                    </ul>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>;
  }
}

export default NavBar;