import React, { Component } from 'react';
import './Footer.css';

class Footer extends Component {
  render(){
    return  <footer id="footer" class="footer color-bg">
                <div class="links-social inner-top-sm">
                    <div class="container">
                        <div class="row">
                            <div class="contact-info">
                                <div class="footer-logo">
                                    <div class="logo">
                                        <a href="#">
                                            <h3>ASA SHOP</h3>
                                        </a>
                                    </div>
                                    <div class="module-body m-t-20">
                                        <p class="about-us"> ASA Shop, cr&eacute;e en 2015 par AMBININTSOA Santatriniaina Alain, si&egrave;ge social &agrave; Fandriana, Lieu d'activit&eacute; dans tout l'ile. Le but de ce portail de vente en ligne est d'aider les petites et moyennes entreprises &agrave; vendre leurs produits dans la grande ile, surtout les produits Vita Malagasy.</p>

                                        <div class="social-icons">

                                            <a href="https://www.facebook.com/ASA-Shop-718023655074575/" class='active'><i class="icon fa fa-facebook"></i></a>
                                            <a href="#"><i class="icon fa fa-twitter"></i></a>
                                            <a href="https://www.linkedin.com/in/shop-asa-143400168/"><i class="icon fa fa-linkedin"></i></a>
                                            <a href="#"><i class="icon fa fa-rss"></i></a>
                                            <a href="#"><i class="icon fa fa-pinterest"></i></a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="info-boxes wow fadeInUp">
                                <div class="info-boxes-inner">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-4 col-lg-4">
                                            <div class="info-box">
                                                <div class="row">
                                                    <div class="col-xs-2">
                                                        <i class="icon fa fa-dollar"></i>
                                                    </div>
                                                    <div class="col-xs-10">
                                                        <h4 class="info-box-heading green">Argent de retour</h4>
                                                    </div>
                                                </div>	
                                                <h6 class="text">Garantie de remboursement de 30 jours.</h6>
                                            </div>
                                        </div>

                                        <div class="hidden-md col-sm-4 col-lg-4">
                                            <div class="info-box">
                                                <div class="row">
                                                    <div class="col-xs-2">
                                                        <i class="icon fa fa-truck"></i>
                                                    </div>
                                                    <div class="col-xs-10">
                                                        <h4 class="info-box-heading orange">Livraison gratuite</h4>
                                                    </div>
                                                </div>
                                                <h6 class="text">Livraison gratuite sur Ar. 1 000 000.00</h6>	
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-sm-4 col-lg-4">
                                            <div class="info-box">
                                                <div class="row">
                                                    <div class="col-xs-2">
                                                        <i class="icon fa fa-gift"></i>
                                                    </div>
                                                    <div class="col-xs-10">
                                                        <h4 class="info-box-heading red">Vente sp&eacute;ciale</h4>
                                                    </div>
                                                </div>
                                                <h6 class="text">Tous les articles-vente jusqu'&agrave; 20% de r&eacute;duction </h6>	
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="contact-information">
                                    <div class="module-heading">
                                        <h4 class="module-title">Opportunit&eacute; pour VOUS!!!</h4>
                                    </div>
                                    <div class="module-body outer-top-xs" >
                                        <ul class="toggle-footer" >
                                            <li class="media">
                                                <div class="pull-left">
                                                    <span class="icon fa-stack fa-lg">
                                                    <i class="fa fa-circle fa-stack-2x"></i>
                                                    <i class="fa fa-check fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                    <span>Devenir collaborateur de ASA Shop</span>
                                                </div>
                                            </li>
                                            <li class="media">
                                                <div class="pull-left">
                                                    <span class="icon fa-stack fa-lg">
                                                    <i class="fa fa-circle fa-stack-2x"></i>
                                                    <i class="fa fa-users fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                    <span>Devenir commercial de ASA Shop</span>
                                                </div>
                                            </li>
                                            <li class="media">
                                                <div class="pull-left">
                                                    <span class="icon fa-stack fa-lg">
                                                    <i class="fa fa-circle fa-stack-2x"></i>
                                                    <i class="fa fa-navicon fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                    <span>Le bon coin de ASA Shop</span>
                                                </div>
                                            </li> 
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="contact-timing">
                                    <div class="module-heading">
                                        <h4 class="module-title">Horaire d'ouverture</h4>
                                    </div>

                                    <div class="module-body outer-top-xs">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <tbody>
                                                    <tr><td>Lundi - vendredi:</td><td class="pull-right">De 08h00 &agrave; 18h00</td></tr>
                                                    <tr><td>Samedi:</td><td class="pull-right">09h00 &agrave; 20h00</td></tr>
                                                    <tr><td>Dimanche:</td><td class="pull-right">De 10.00 &agrave; 20.00</td></tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="contact-information">
                                    <div class="module-heading">
                                        <h4 class="module-title">Information</h4>
                                    </div>
                                    <div class="module-body outer-top-xs" >
                                        <ul class="toggle-footer" >
                                            <li class="media">
                                                <div class="pull-left">
                                                    <span class="icon fa-stack fa-lg">
                                                    <i class="fa fa-circle fa-stack-2x"></i>
                                                    <i class="fa fa-map-marker fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                    <span>Antananarivo, Madagascar</span>
                                                </div>
                                            </li>
                                            <li class="media">
                                                <div class="pull-left">
                                                    <span class="icon fa-stack fa-lg">
                                                    <i class="fa fa-circle fa-stack-2x"></i>
                                                    <i class="fa fa-mobile fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                </div>
                                                <div class="media-body">
                                                    <p>(+261) 34 81 84 201<br/>(+261) 34 81 84 201</p>
                                                </div>
                                                <div class="pull-left">
                                                    <span class="icon fa-stack fa-lg">
                                                    <i class="fa fa-circle fa-stack-2x"></i>
                                                    <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                    <span><a href="#">commercial@asahop.com</a></span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </footer>;
  }
}

export default Footer;